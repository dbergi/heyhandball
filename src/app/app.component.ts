import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import {TranslateService} from '@ngx-translate/core';
import {ScreenOrientation} from '@ionic-native/screen-orientation/ngx';
import { Insomnia } from '@ionic-native/insomnia/ngx';
import { AppRate } from '@ionic-native/app-rate/ngx';
import {StoreService} from './services/store.service';
import {Globalization} from '@ionic-native/globalization/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private translate: TranslateService,
    private screenOrientation: ScreenOrientation,
    private insomnia: Insomnia,
    private appRate: AppRate,
    private store: StoreService,
    private globalization: Globalization,
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.translate.setDefaultLang('en');
      this.insomnia.keepAwake();

      if (this.platform.is('android')) {
        this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
        this.store.setStore();
      }

      this.globalization.getPreferredLanguage()
          .then(res => {
            const lang = res.value.slice(0, res.value.indexOf('-'));
            console.log(lang);
            if (lang === 'es') {
              this.translate.use('es');
            }
            this.rate();
          })
          .catch(e => console.log(e));
    });
  }

  private async rate() {
    const customLocale = {
      title: await this.translate.get('app.rate-title').toPromise(),
      message: await this.translate.get('app.rate-message').toPromise(),
      cancelButtonLabel: await this.translate.get('app.rate-cancelButton').toPromise(),
      laterButtonLabel: await this.translate.get('app.rate-laterButton').toPromise(),
      rateButtonLabel: await this.translate.get('app.rate-rateButton').toPromise(),
      yesButtonLabel: await this.translate.get('app.rate-yes').toPromise(),
      noButtonLabel: await this.translate.get('app.rate-no').toPromise(),
      appRatePromptTitle: await this.translate.get('app.rate-promptTitle').toPromise(),
      feedbackPromptTitle: await this.translate.get('app.rate-feedbackTitle').toPromise()
    }
    this.appRate.preferences = {
      ...this.appRate.preferences,
      usesUntilPrompt: 3,
      simpleMode: true,
      useLanguage: 'es',
      showPromptForInAppReview: true,
      promptAgainForEachNewVersion: true,
      storeAppURL: {
        ios: 'com.bergitech.heyhandball',
        // ios: '1523888022',
        android: 'market://details?id=com.bergitech.heyhandball',
        windows: 'ms-windows-store://review/?ProductId=9NHP2F7DJWCS'
      },
      customLocale,
    };

    this.appRate.promptForRating(false);
  }
}
