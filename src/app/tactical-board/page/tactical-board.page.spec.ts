import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TacticalBoardPage } from './tactical-board.page';

describe('TacticalBoardPage', () => {
  let component: TacticalBoardPage;
  let fixture: ComponentFixture<TacticalBoardPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TacticalBoardPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TacticalBoardPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
