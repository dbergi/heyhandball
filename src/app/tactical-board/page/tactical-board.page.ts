import {Component, ElementRef, ViewChild} from '@angular/core';
import Konva from 'konva';
import {ResizedEvent} from 'angular-resize-event';
import {ModalController, Platform, PopoverController, ToastController} from '@ionic/angular';
import {ElementPopoverComponent} from '../element-popover/element-popover.component';
import {of} from 'rxjs';
import {KonvaComponent} from 'ng2-konva';
import {LoadPopoverComponent} from '../load-popover/load-popover.component';
import {SaveModalPage} from '../save-modal/save-modal.page';
import {Storage} from '@ionic/storage';
import {TranslateService} from '@ngx-translate/core';
import {isEqual} from 'lodash';
import {OrderPage} from '../../settings/order/order.page';
import {VersionService} from '../../services/version.service';
import {FileDownloadService} from '../../services/file-download.service';
import {SocialSharingService} from '../../services/social-sharing.service';
import {HelpersService} from '../../services/helpers.service';
import {SavePopoverComponent} from '../save-popover/save-popover.component';
import {RecorderService} from '../../services/recorder.service';
import {SettingsService} from '../../services/settings.service';

@Component({
  selector: 'app-tactical-board',
  templateUrl: './tactical-board.page.html',
  styleUrls: ['./tactical-board.page.scss'],
})
export class TacticalBoardPage {
  @ViewChild('container', {static: false}) containerEl: ElementRef;
  @ViewChild('stage', {static: false}) stageEl: KonvaComponent;
  @ViewChild('layer', {static: false}) layerEl: KonvaComponent;
  @ViewChild('court', {static: false}) courtEl: KonvaComponent;
  @ViewChild('toggleLineTypesElement', {static: true, read: ElementRef}) toggleLineTypesElement: ElementRef;
  @ViewChild('toggleTrainingStuffElement', {static: true, read: ElementRef}) toggleTrainingStuffElement: ElementRef;

  private subscriptions = [];

  public groupDraw;

  public configStage = of({});
  public configCourt = of({});

  private stage;
  private layer;
  private court;

  private courtWidth;
  private courtHeight;
  private rotate = false;
  private scale;
  private radius = 20;
  public lineType = 1;
  public lineColor = '#000000';

  public toggleLineTypes = false;
  public toggleTrainingStuff = false;
  public showAnimationToolbar  = false;
  public toggleColorPicker = false;

  public jsonSteps = [];
  public step = 0;

  public jsonFrames = [];
  public frame = 0;

  public tweenDuration = 1;

  private states = [];
  private animations = [];

  public isPlaying = false;
  public isPaused = false;
  public recording = false;
  private tweens;

  public trainingStuff = [
    'barrier',
    'barrier_single',
    'cone',
    'hoop'
  ];

  public tooltips = [];
  public activeTooltip = false;
  public currentTooltip = 0;
  
  private settings;
  private containerWidth: number;
  private containerHeight: number;

  constructor(
    public popoverController: PopoverController,
    private modalController: ModalController,
    private toastController: ToastController,
    private storage: Storage,
    public translate: TranslateService,
    public platform: Platform,
    private version: VersionService,
    private fileDownload: FileDownloadService,
    private socialShare: SocialSharingService,
    private helpers: HelpersService,
    private recorder: RecorderService,
    private settingsService: SettingsService,
  ) {
    Konva.autoDrawEnabled = true;
    this.subscriptions.push(this.platform.backButton.subscribe(() => {
      navigator['app'].exitApp();
    }));
  }

  ionViewWillEnter() {
    this.stage = this.stageEl.getStage();
    this.layer = this.layerEl.getStage();
    this.court = this.courtEl.getStage();
    this.court.listening(false);
    this.subscriptions.push(this.settingsService.watch().subscribe(settings => {
      this.settings = settings;
      this.storage.get('states').then(states => this.states = states || []);
      this.storage.get('animations').then(animations => this.animations = animations || []);
      // this.containerEl.nativeElement.dispatchEvent(new Event('resize'));
    }));
    this.createBasicElements();
  }

  ionViewWillLeave() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  private createBasicElements() {
    const imageObj = new Image();

    imageObj.onload = () => {
      this.courtWidth = imageObj.width;
      this.courtHeight = imageObj.height;

      this.layer.setAttrs({
        offsetX: imageObj.width / 2,
        offsetY: imageObj.height / 2,
        size: {width: imageObj.width, height: imageObj.height}
      });

      this.court.setAttrs({
        image: imageObj,
        fillEnabled: false,
        shadowEnabled: true,
        shadowOffset: {x: 0, y: 0},
        shadowBlur: 3,
        shadowColor: '#fff',
      });
      this.court.cache();
      this.addGroupDraw();
    };
    imageObj.src = 'assets/image/' + this.settings.court;
    this.allowLineDrawing();
  }

  public addGroupDraw() {
    if (this.groupDraw) {
      this.groupDraw.destroyChildren();
    } else {
      this.groupDraw = new Konva.Group();
      this.layer.add(this.groupDraw);
    }
    this.step = 0;
    this.jsonSteps = [];

    this.addPlayer('1');
    this.addPlayer('2');
    this.addBall();
  }

  private allowLineDrawing() {
    this.stage.on('touchstart mousedown', event => {
      if (event.target.nodeType === 'Stage') {

        const line = this.drawLine(event);

        this.stage.on('mouseup touchend', ev => {
          this.stage.off('mousemove touchmove mouseup touchend');
          this.addLineEvents(line);

          this.saveStep();
        });

        this.stage.on('mousemove touchmove', ev => {
          const {x, y} = this.layer.getRelativePointerPosition();
          line.points().push(x, y);
          this.layer.batchDraw();
        });

        line.moveToBottom();
      }
    });
  }

  private drawLine(event) {
    const {x, y} = this.layer.getRelativePointerPosition();
    const line = new Konva[this.lineType > 2 ? 'Arrow' : 'Line']({
      name: 'line',
      points: [x, y],
      stroke: this.lineColor,
      strokeWidth: this.radius / 6,
      originalStrokeWidth: this.radius / 6,
      shadowForStrokeEnabled: false,
      tension: 0,
      dash: [2, 4, 6].includes(this.lineType) ? [this.radius / 2] : [],
      bezier: true,
      lineCap: 'round',
      pointerLength: this.radius / 2,
      pointerWidth: this.radius,
      fill: this.lineColor,
      pointerAtBeginning: this.lineType > 4,
      draggable: true,
      hitStrokeWidth: 20,
    });

    line.id(line._id.toString());

    this.groupDraw.add(line);

    return line;
  }

  private addLineEvents(line) {
    line.on('dblclick dbltap', async event => {
      const popover = await this.showElementPopOver(event, line);
      popover.onDidDismiss().then(() => this.saveStep());
    });

    line.on('dragend', () => {
      this.saveStep();
    });

    line.moveToBottom();
    line.cache({
      imageSmoothingEnabled: true
    });
  }

  private addPlayer(team) {
    const circle = new Konva.Circle({
      radius: this.radius,
      fillRadialGradientStartRadius: 0,
      fillRadialGradientEndRadius: this.radius * 4,
      fillRadialGradientColorStops: team === '1' ? [0, this.settings.teamAColor, 1, 'black'] : [0, this.settings.teamBColor, 1, 'black'],
      // stroke: 'white',
      // strokeWidth: this.playerStrokeWidth,
      shadowForStrokeEnabled: false,
      shadowOffset: {x: 0, y: 0},
      shadowBlur: 10,
      hitStrokeWidth: 20
    });

    const playerNum = new Konva.Text({
      text: this.nextPlayerNumber(team).toString(),
      width: this.radius * 2,
      height: this.radius * 2,
      fill: 'white',
      fontSize: this.radius,
      fontStyle: 'bold',
      align: 'center',
      verticalAlign: 'middle',
      offset: {
        x: this.radius,
        y: this.radius,
      }
    });

    const playerGroup = new Konva.Group({
      name: team,
      width: this.radius * 2,
      height: this.radius * 2,
      opacity: 0.3,
      draggable: true,
      dragBoundFunc: (pos) => {
        return {
          x: pos.x,
          y: pos.y - this.radius
        };
      },
      x: this.court.width() - this.radius - 5,
      y: this.court.height() / 2 + this.radius * (team === '1' ? -2.5 : 2.5),
      id: team + '-' + playerNum.text(),
      scale: {
        x: this.settings.elementsScale,
        y: this.settings.elementsScale,
      },
      rotation: this.rotate ? 90 : 0
    });

    playerGroup.add(circle);
    playerGroup.add(playerNum);

    this.groupDraw.add(playerGroup);

    this.addPlayerEvents(playerGroup, team);
  }

  addPlayerEvents(player, team) {
    player.on('dragend', () => {
      if (!this.insideCourt(player)) {
        this.changeNumberOfPlayerInBench(player);
        player.destroy();
      }
      this.saveStep();
    });

    player.on('dragstart', ev => {
      if (!player.attrs.firstDrag) {
        player.opacity(1.00000000001);
        player.attrs.firstDrag = true;

        this.addPlayer(team);
      }
    });

    player.on('dblclick dbltap', async event => {
      const popover = await this.showElementPopOver(event, player);
      popover.onDidDismiss().then(() => this.saveStep());
    });

    player.moveToTop();
    player.moveDown();
    player.cache({
      imageSmoothingEnabled: true
    });
  }

  private changeNumberOfPlayerInBench(player) {
    const benchPlayer = this.groupDraw.findOne(node => {
      return node.getType() === 'Group' && !node.attrs.firstDrag && node.name() === player.name();
    });

    const playerText = player.findOne(child => {
      return child.getClassName() === 'Text';
    }).text();

    benchPlayer.clearCache();
    const benchPlayerText = benchPlayer.findOne(child => {
      return child.getClassName() === 'Text';
    });

    if (benchPlayerText.text() > playerText) {
      benchPlayerText.text(playerText);
    }

    benchPlayer.cache({
      pixelRatio: 3,
      imageSmoothingEnabled: true,
    });
  }

  private nextPlayerNumber(team) {
    const numbersUsed = [];

    this.groupDraw.find('.' + team).forEach(player => {
      numbersUsed.push(parseInt(player.findOne('Text').text(), 10));
    });

    let numberFound = false, playerNumber = 0;

    while (numberFound === false) {
      if (!numbersUsed.includes(++playerNumber)) {
        numberFound = true;
        return playerNumber;
      }
    }
  }

  private addBall() {
    const imageObj = new Image();

    imageObj.onload = () => {
      const ball = new Konva.Image({
        name: 'ball',
        image: imageObj,
        src: imageObj.src,
        width: this.radius,
        height: this.radius,
        offset: {
          x: this.radius / 2,
          y: this.radius / 2
        },
        x: this.court.width() - this.radius - 5,
        y: this.court.height() / 2,
        draggable: true,
        dragBoundFunc: (pos) => {
          return {
            x: pos.x,
            y: pos.y - this.radius
          };
        },
        shadowForStrokeEnabled: false,
        opacity: 0.5,
        shadowOffset: {x: 0, y: 0},
        shadowBlur: 10,
        hitStrokeWidth: 20,
        scale: {
          x: this.settings.elementsScale,
          y: this.settings.elementsScale,
        }
      });

      ball.id(ball._id.toString());

      this.addBallEvents(ball);
      this.groupDraw.add(ball);
      ball.moveToTop();
      this.scaleElements();
    };

    imageObj.src = 'assets/icon/' + this.settings.ball;
  }

  private addBallEvents(ball) {
    ball.on('dragend', () => {
      if (!this.insideCourt(ball)) {
        ball.opacity(0.5);
        ball.position({
          x: this.court.width() -  this.radius - 5,
          y: this.court.height() / 2
        });
      }
      this.saveStep();
    });

    ball.on('dragstart', ev => {
      ball.opacity(1.1);
    });

    ball.on('dblclick dbltap', async ev => {
      const popover = await this.showElementPopOver(ev, ball, true);
      popover.onDidDismiss().then(() => this.saveStep());
    });

    ball.cache({
      pixelRatio: 3,
        imageSmoothingEnabled: true,
    });
  }

  public addTrainingStuff(event, object) {
    event.stopPropagation();

    const imageObj = new Image();

    imageObj.onload = () => {
      const im = new Konva.Image({
        object,
        name: 'trainingStuff',
        image: imageObj,
        src: imageObj.src,
        width: this.radius * (['barrier.svg', 'barrier_single.svg'].includes(object) ? 3 : 2),
        height: this.radius * (['barrier.svg', 'barrier_single.svg'].includes(object) ? 3 : 2),
        offset: {
          x: this.radius * (['barrier.svg', 'barrier_single.svg'].includes(object) ? 3 : 2) /  2,
          y: this.radius * (['barrier.svg', 'barrier_single.svg'].includes(object) ? 3 : 2) / 2,
        },
        shadowForStrokeEnabled: false,
        position: {x: this.court.width() / 2, y: this.court.height() / 2},
        draggable: true,
        dragBoundFunc: (pos) => {
          return {
            x: pos.x,
            y: pos.y - this.radius
          };
        },
        shadowOffset: {x: 0, y: 0},
        shadowBlur: 10,
        hitStrokeWidth: 20,
        scale: {
          x: this.settings.elementsScale,
          y: this.settings.elementsScale,
        },
        rotation: this.rotate ? 90 : 0
      });

      this.groupDraw.add(im);
      im.id(im._id.toString(10));

      this.addTrainingStuffEvents(im);

      this.saveStep();
    };

    imageObj.src = 'assets/icon/' + object;
  }

  private addTrainingStuffEvents(element) {
    element.on('dragend', () => {
      if (!this.insideCourt(element)) {
        element.destroy();
      }
      this.saveStep();
    });

    element.on('dblclick dbltap', async ev => {
      const popover = await this.showElementPopOver(ev, element);
      popover.onDidDismiss().then(() => this.saveStep());
    });

    element.moveDown();
    element.cache({
      pixelRatio: 3,
      imageSmoothingEnabled: true,
    });
  }

  private async showElementPopOver(ev: any, element, disableDelete = false) {
    const popover = await this.popoverController.create({
      component: ElementPopoverComponent,
      translucent: true,
      componentProps: {
        element,
        disableDelete
      },
      cssClass: 'elementPopover'
    });

    await popover.present();
    return popover;
  }

  private calcScale(width, height) {
    this.rotate =  width >  height;
    this.scale = this.rotate ? width / this.courtHeight : height / this.courtHeight;

    if (this.courtWidth * this.scale > (this.rotate ? height : width)) {
      this.scale = this.rotate ? height / this.courtWidth : width / this.courtWidth;
    }
  }

  public onResize(event: ResizedEvent) {
    this.containerWidth = event.newRect.width;
    this.containerHeight = event.newRect.height;

    this.scaleElements();
  }

  private scaleElements() {
    if (this.stage) {
      this.calcScale(this.containerWidth, this.containerHeight);

      this.stage.setAttrs({
        width: this.containerWidth,
        height: this.containerHeight
      });

      this.layer.setAttrs({
        scale: {x: this.scale, y: this.scale},
        position: {x: this.containerWidth / 2, y: this.containerHeight / 2},
        rotation: this.rotate ? -90 : 0
      });

      this.groupDraw.getChildren().forEach((child) => {
        if (child.name() !== 'line') {
          child.rotation(this.rotate ? 90 : 0);
        }
        child.cache();
      });
    }
  }

  private insideCourt(shape) {
    const x = shape.x();
    const y = shape.y();
    const courtWidth = this.court.width();
    const courtHeight = this.court.height();

    return x > 0 && x < courtWidth && y > 0 && y < courtHeight;
  }

  private redrawImage(image) {
    const imageObj = new Image();
    let im;

    imageObj.onload = () => {
      im = new Konva.Image({...image.getAttrs(), ...{image: imageObj}});
      image.destroy();

      this.groupDraw.add(im);
      if (im.attrs.name === 'ball') {
        this.addBallEvents(im);
        im.moveToTop();
      } else {
        this.addTrainingStuffEvents(im);
      }
    };

    imageObj.src = image.attrs.src;

  }

  public saveStep(step = 'step') {
    this['json' + this.capitalize(step) + 's'][this[step]] = this.groupDraw.toJSON();
    this['json' + this.capitalize(step) + 's'].splice(this[step] + 1);
    this[step]++;

    if (step === 'frame') {
      this.translate.get('tactical-board.frame-captured').toPromise().then(message => {
        this.presentToast(message);
      });
    }
  }

  private async presentToast(message, duration = 1000) {
    const toast = await this.toastController.create({
      message,
      duration,
      position: 'middle',
      color: 'success'
    });
    await toast.present();
    return toast;
  }

  public undoStep() {
    this.step--;
    this.loadStep('step');
  }

  private loadStep(step) {
    this.groupDraw.destroy();
    this.groupDraw = Konva.Node.create(this['json' + this.capitalize(step) + 's'][this[step] - 1]);
    this.layer.add(this.groupDraw);
    this.addEventsAndRedrawImages();
  }

  public redoStep() {
    this.step++;
    this.loadStep('step');
  }

  private addEventsAndRedrawImages() {
    this.groupDraw.find('.1, .2').forEach(player => {
      this.addPlayerEvents(player, player.name());
    });

    this.groupDraw.find('Line, Arrow').forEach(line => {
      this.addLineEvents(line);
    });

    this.groupDraw.find('Image').map(image => {
      this.redrawImage(image);
    });
  }

  public clearLines() {
    this.groupDraw.find('Line, Arrow').forEach(line => {
      line.destroy();
    });
    this.saveStep();
  }

  public toggleAnimation() {
    this.showAnimationToolbar = !this.showAnimationToolbar;
    if (this.settings.showTutorial && this.showAnimationToolbar) {
      this.startTutorial();
    }
  }

  private startTutorial() {
    this.currentTooltip = 0;
    this.nextTooltip();
  }

  private async nextTooltip() {
    this.currentTooltip++;

    if (this.activeTooltip) {
      await this.hideTooltip();
    }

    const translation = this.translate.instant('tactical-board.tutorial.' + this.currentTooltip);
    if (translation !== 'tactical-board.tutorial.' + this.currentTooltip) {

      setTimeout(() => {
        this.tooltips[this.currentTooltip] = translation;
        this.activeTooltip = true;

        setTimeout(() => {
          const tooltip = document.getElementsByTagName('tooltip-box').item(0);
          tooltip.addEventListener('click', this.nextTooltip.bind(this), false);
        }, 10);
      }, 400);
    } else {
      this.settingsService.setValue('showTutorial', false);
    }
  }

  private async hideTooltip() {
    const tooltip = document.getElementsByTagName('tooltip-box').item(0);
    tooltip.removeEventListener('click', this.nextTooltip);
    this.activeTooltip = false;
  }

  public toggleFab(type) {
    this.toggleLineTypes = false;
    this.toggleTrainingStuff = false;
    this[type] = this[type + 'Element'].nativeElement.activated;

    setTimeout(() => {
      this[type] = !this[type + 'Element'].nativeElement.activated;
    }, 1);
  }

  public async downloadStage() {
    const dataURL = this.stage.toDataURL({ pixelRatio: 3 });
    return this.fileDownload.download(dataURL, 'stage_' + this.helpers.getDate())
        .then((path) => {
          this.translate.get('tactical-board.image-saved', {path}).subscribe(message => {
            this.presentToast(message, 3000);
          });
        });
  }

  public async share() {
    const sharingType = await this.presentSelectPopover('share', this.jsonFrames.length < 2);
    if (sharingType.data === 'screenshot') {
      this.shareStage();
    }
    if (sharingType.data === 'animation') {
      this.shareAnimation();
    }
  }

  public async shareStage() {
    const dataURL = this.stage.toDataURL({ pixelRatio: 3 });
    await this.socialShare.share(dataURL);
  }

  public async shareAnimation() {
    this.showAnimationToolbar = true;

    this.recording = await this.recorder.startRecording(this.layer);
    if (this.recording) {
      this.playAnimation();
    }
  }

  public async save() {
    const savingType = await this.presentSelectPopover('save', this.jsonFrames.length < 2);
    if (savingType.data === 'screenshot') {
      this.saveState();
    }
    if (savingType.data === 'animation') {
      this.saveAnimation();
    }
  }

  async presentSelectPopover(type, disableAnimationSaving = false) {
    const popover = await this.popoverController.create({
      component: SavePopoverComponent,
      componentProps: {
        type,
        disableAnimationSaving
      },
      translucent: true
    });

    await popover.present();
    return await popover.onDidDismiss();
  }

  public async saveState() {
    if (this.states.reduce((count, row) => count + row.length, 0) >= 2 && !this.version.fullVersion.getValue()) {
      this.order();
    } else {
      const data = await this.presentSaveModal(this.states);

      if (data.data) {
        const state = {
          draw: this.groupDraw.toJSON(),
          image: this.stage.toDataURL(),
          name: data.data.name || (new Date()).toLocaleString(),
          folder: data.data.folder || '-',
          settings: this.settings,
        };

        this.pushToArrayByFolder(this.states, state);
        this.storage.set('states', this.states);

        this.translate.get('tactical-board.state-captured').toPromise().then(message => {
          this.presentToast(message);
        });
      }
    }
  }

  private pushToArrayByFolder(elements, element) {
    const index = elements.findIndex(el => {
      return el[0].folder === element.folder;
    });

    if (index !== -1) {
      elements[index].push(element);
    } else {
      elements.push([element]);
    }
  }

  async presentSaveModal(elements) {
    const modal = await this.modalController.create({
      component: SaveModalPage,
      cssClass: 'saveModal',
      componentProps: {
        saveName: null,
        folderName: null,
        date: (new Date()).toLocaleString(),
        folders: elements.map(e => e[0].folder)
      }
    });
    await modal.present();
    return await modal.onDidDismiss();
  }

  public async load() {
    const savingType = await this.presentSelectPopover('load');
    if (savingType.data === 'screenshot') {
      this.loadState();
    }
    if (savingType.data === 'animation') {
      this.loadAnimation();
    }
  }

  public async loadState() {
    const popover = await this.presentLoadPopover(this.states);
    await popover.onDidDismiss().then(data => {
      this.storage.set('states', this.states);

      if (data.data !== undefined) {
        this.jsonSteps = [this.states[data.data.index][data.data.index2]];
        this.step = 0;
        this.redoStep();
      }
    });
  }

  private async presentLoadPopover(elements) {
    const popover = await this.popoverController.create({
      component: LoadPopoverComponent,
      componentProps: {
        elements
      },
      cssClass: 'loadPopover',
      translucent: true
    });
    await popover.present();
    return popover;
  }

  public frameBack() {
    this.frame--;
    this.loadStep('frame');
  }

  public frameForward() {
    this.frame++;
    this.loadStep('frame');
  }

  public async loadAnimation() {
    const popover = await this.presentLoadPopover(this.animations);
    await popover.onDidDismiss().then(data => {
      this.storage.set('animations', this.animations);
      if (data.data !== undefined) {
        this.jsonFrames = this.animations[data.data.index][data.data.index2].frames;
        this.frame = -1;
        this.frameForward();
      }
    });
  }

  public async saveAnimation() {
    if (this.animations.reduce((count, row) => count + row.length, 0) >= 2 && !this.version.fullVersion.getValue()) {
      this.order();
    } else {
      const data = await this.presentSaveModal(this.animations);
      if (data.data) {
        const animation = {
          frames: this.jsonFrames,
          name: data.data.name || (new Date()).toLocaleString(),
          folder: data.data.folder || '-',
          settings: this.settings,
        };
        this.pushToArrayByFolder(this.animations, animation);
        this.storage.set('animations', this.animations);

        this.translate.get('tactical-board.animation-captured').toPromise().then(message => {
          this.presentToast(message);
        });
      }
    }
  }

  public async order() {
    const modal = await this.modalController.create({
      component: OrderPage,
      cssClass: 'orderModal'
    });

    await modal.present();
    await modal.onDidDismiss();
  }

  public playAnimation() {
    if (this.isPlaying) {
      this.pauseResumeTweens();
    } else {
      this.addTweens();
    }
  }

  private pauseResumeTweens() {
    if (this.isPaused) {
      this.resumeTweens();
    } else {
      this.pauseTweens();
    }
  }

  private resumeTweens() {
    this.tweens.forEach(tween => {
      tween.play();
    });

    this.isPaused = false;
  }

  private pauseTweens() {
    this.tweens.forEach(tween => {
      tween.pause();
    });

    this.isPaused = true;
  }

  private addTweens() {
    this.frame = 1;
    this.loadStep('frame');
    this.addFrameTweens(this.frame);
  }

  private addFrameTweens(frame) {
    const nextGroupDraw = Konva.Node.create(this.jsonFrames[frame++]);

    nextGroupDraw.getChildren().forEach( child => {
      if (!this.groupDraw.findOne('#' + child.id())) {
        this.addMissingElement(child);
      }
    });

    this.tweens = [];

    this.groupDraw.getChildren().forEach(child => {
      this.addElementTween(child, nextGroupDraw, frame);
    });

    this.playTweens();

    nextGroupDraw.destroy();
  }

  private addMissingElement(child) {
    const clone = child.clone();
    this.groupDraw.add(clone);

    switch (clone.name()) {
      case '1':
      case '2':
        this.addPlayerEvents(clone, clone.name());
        break;
      case 'line':
        this.addLineEvents(clone);
        break;
      case 'trainingStuff':
        this.redrawImage(clone);
        break;
    }
  }

  private addElementTween(child, nextGroupDraw, frame) {
    try {
      const nextAttributes = nextGroupDraw.findOne('#' + child.id()).getAttrs();

      if (!isEqual(nextAttributes, child.getAttrs())) {
        delete nextAttributes.id;
        delete nextAttributes.name;
        delete nextAttributes.object;

        this.tweens.push(child.tween = new Konva.Tween({
          ...nextAttributes,
          node: child,
          duration: this.tweenDuration,
          easing: Konva.Easings[this.settings.easing],
          onFinish: () => {
            if (this.frame < frame) {
              this.frame++;

              if (frame === this.jsonFrames.length - 1) {
                this.isPlaying = false;
                if (this.recording) {
                  setTimeout(async () => {
                    this.recording = await this.recorder.stopRecording();
                  }, 500);
                }
              } else {
                this.addFrameTweens(frame);
              }
            }
          }
        }));

      }
    } catch (e) {
      child.destroy();
    }
  }

  private playTweens() {
    this.tweens.forEach(tween => {
      tween.play();
    });

    this.isPlaying = true;
  }

  public clearAnimation() {
    this.jsonFrames = [];
    this.frame = -1;
    this.groupDraw.show();
  }

  private capitalize = (s) => {
    if (typeof s !== 'string') { return ''; }
    return s.charAt(0).toUpperCase() + s.slice(1);
  }

  public changeTweenDuration() {
    if (this.tweenDuration === 2) {
      this.tweenDuration = 0;
    }
    this.tweenDuration += 0.5;
  }
}
