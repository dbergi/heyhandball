import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TacticalBoardPage } from './tactical-board.page';
import {RouterModule} from '@angular/router';
import {AngularResizeEventModule} from 'angular-resize-event';
import {ElementPopoverComponent} from '../element-popover/element-popover.component';
import {KonvaModule} from 'ng2-konva';
import {TranslateModule} from '@ngx-translate/core';
import {ColorPickerModule} from 'ngx-color-picker';
import {LoadPopoverComponent} from '../load-popover/load-popover.component';
import {SaveModalPageModule} from '../save-modal/save-modal.module';
import {TooltipsModule} from 'ionic4-tooltips';
import {OrderPageModule} from '../../settings/order/order.module';
import {SavePopoverComponent} from '../save-popover/save-popover.component';

@NgModule({
    imports: [
        IonicModule,
        CommonModule,
        FormsModule,
        RouterModule.forChild([{ path: '', component: TacticalBoardPage }]),
        AngularResizeEventModule,
        KonvaModule,
        TranslateModule,
        ColorPickerModule,
        SaveModalPageModule,
        TooltipsModule.forRoot(),
        OrderPageModule,
    ],
    declarations: [TacticalBoardPage, ElementPopoverComponent, LoadPopoverComponent, SavePopoverComponent]
})
export class TacticalBoardPageModule {}
