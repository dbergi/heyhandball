import { Component, OnInit } from '@angular/core';
import {ModalController, PopoverController} from '@ionic/angular';
import {SaveModalPage} from '../save-modal/save-modal.page';
import {Storage} from "@ionic/storage";

@Component({
  selector: 'app-load-popover',
  templateUrl: './load-popover.component.html',
  styleUrls: ['./load-popover.component.scss'],
})
export class LoadPopoverComponent implements OnInit {

  elements;

  public reorder = false;
  public groupVisibility = [];

  constructor(
    private popoverController: PopoverController,
    private modalController: ModalController,
  ) { }

  ngOnInit() {}

  public select(index, index2) {
    this.popoverController.dismiss({index, index2});
  }

  public async edit(index, index2) {
    const data = await this.presentSaveModal(index, index2);
    this.elements[index][index2].name = data.data.name || (new Date()).toLocaleString();

    if (this.elements[index][index2].folder !== data.data.folder) {
      this.elements[index][index2].folder = data.data.folder || '-';
      this.pushToArrayByFolder(this.elements, this.elements[index][index2]);
      this.elements[index].splice(index2, 1);

      if (this.elements[index].length === 0) {
        this.elements.splice(index, 1);
      }
    }
  }

  public deleteItem(index, index2) {
    this.elements[index].splice(index2, 1);

    if (!this.elements[index].length) {
      this.deleteGroup(index);
    }
  }

  public deleteGroup(index) {
    this.elements.splice(index, 1);
  }

  public reorderItems(ev: any, index) {
    ev.stopPropagation();
    this.elements[index].splice(ev.detail.to, 0, this.elements[index].splice(ev.detail.from, 1)[0]);

    ev.detail.complete();
  }

  public reorderGroups(ev: any) {
    this.elements.splice(ev.detail.to, 0, this.elements.splice(ev.detail.from, 1)[0]);
    const visibleDestinationIndex = this.groupVisibility[ev.detail.to];
    this.groupVisibility[ev.detail.to] = this.groupVisibility[ev.detail.from];
    this.groupVisibility[ev.detail.from] = visibleDestinationIndex;

    ev.detail.complete();
  }

  async presentSaveModal(index, index2) {
    const modal = await this.modalController.create({
      component: SaveModalPage,
      cssClass: 'saveModal',
      componentProps: {
        saveName: this.elements[index][index2].name,
        folderName: this.elements[index][index2].folder
      }
    });

    await modal.present();
    return await modal.onDidDismiss();
  }

  private pushToArrayByFolder(elements, element) {
    const index = elements.findIndex(el => {
      return el[0].folder === element.folder;
    });

    if (index !== -1) {
      elements[index].push(element);
    } else {
      elements.push([element]);
    }
  }
}
