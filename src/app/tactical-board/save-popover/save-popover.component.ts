import { Component, OnInit } from '@angular/core';
import {PopoverController} from '@ionic/angular';

@Component({
  selector: 'app-save-popover',
  templateUrl: './save-popover.component.html',
  styleUrls: ['./save-popover.component.scss'],
})
export class SavePopoverComponent implements OnInit {

  type;
  disableAnimationSaving;

  constructor(public popover: PopoverController) { }

  ngOnInit() {}

}
