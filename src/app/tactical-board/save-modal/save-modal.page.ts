import { Component, OnInit } from '@angular/core';
import {ModalController} from '@ionic/angular';
import {FoldersAutocompleteService} from "../../services/folders-autocomplete.service";

@Component({
  selector: 'app-save-modal',
  templateUrl: './save-modal.page.html',
  styleUrls: ['./save-modal.page.scss'],
})
export class SaveModalPage implements OnInit {

  saveName;
  folderName;
  date;
  folders;

  constructor(
    private modalController: ModalController,
    public foldersAutocompleteService: FoldersAutocompleteService,
  ) { }

  ngOnInit() {
    this.foldersAutocompleteService.setFolders(this.folders);
  }

  save() {
    this.modalController.dismiss({name: this.saveName, folder: this.folderName});
  }

  protected filter(keyword) {
    keyword = keyword.toLowerCase();

    return this.folders.filter(folder => {
        const value = folder.toLowerCase();

        return value.includes(keyword);
      }
    );
  }
}
