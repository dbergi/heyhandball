import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SaveModalPage } from './save-modal.page';
import {TranslateModule} from '@ngx-translate/core';
import { AutoCompleteModule } from 'ionic4-auto-complete';

@NgModule({
    imports: [
        AutoCompleteModule.forRoot(),
        CommonModule,
        FormsModule,
        IonicModule,
        TranslateModule
    ],
    declarations: [SaveModalPage]
})
export class SaveModalPageModule {}
