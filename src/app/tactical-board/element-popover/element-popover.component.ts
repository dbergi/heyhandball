import { Component, OnInit } from '@angular/core';
import {PopoverController} from '@ionic/angular';
import Konva from 'konva';

@Component({
  selector: 'app-element-popover',
  templateUrl: './element-popover.component.html',
  styleUrls: ['./element-popover.component.scss'],
})
export class ElementPopoverComponent implements OnInit {
  element;
  disableDelete;

  public colorPickerToggle = false;
  public fillColor;

  constructor(private popoverController: PopoverController) { }

  ngOnInit() {}

  sizeChange(event) {
    if (this.element.getClassName() === 'Line' || this.element.getClassName() === 'Arrow') {
      this.element.clearCache();
      this.element.strokeWidth(this.element.attrs.originalStrokeWidth * event.target.value);
      this.element.cache();
    } else {
      this.element.scale({
        x: event.target.value,
        y: event.target.value
      });
    }
    this.element.getLayer().draw();
  }

  deleteElement() {
    if (this.element.getClassName() === 'Group') {
      this.changeNumberOfPlayerInBench(this.element);
    }

    const layer = this.element.getLayer();
    this.element.destroy();
    layer.draw();
    this.popoverController.dismiss(true);
  }

  selectColor(color) {
    this.element.clearCache();

    if (this.element.getClassName() === 'Image') {
      color = color.substring(4, color.length - 1).split(',');
      this.element.cache();
      this.element.filters([Konva.Filters.RGB]);
      this.element.red(color[0]);
      this.element.green(color[1]);
      this.element.blue(color[2]);
      this.element.alpha(color[3]);
    } else if (this.element.getClassName() === 'Group') {
      this.element.findOne('Circle').fillRadialGradientColorStops([0, color, 1, 'black']);
    } else {
      this.element.stroke(color);
    }
    this.element.cache();
    this.element.getLayer().batchDraw();
  }

  hexToRgb(hex: string) {
    hex = hex.replace(/^#/, '');
    // support short-version with one nybble per chroma by expanding 'x' to 'xx'
    hex = hex.length === 3 ? hex.replace(/(.)/g, '$1$1') : hex;

    return {
      r: parseInt(hex.substr(0, 2), 16) / 255,
      g: parseInt(hex.substr(2, 2), 16) / 255,
      b: parseInt(hex.substr(4, 2), 16) / 255
    };
  }

  public rotate() {
    this.element.rotate(45);
    this.element.getLayer().draw();
  }

  private changeNumberOfPlayerInBench(player) {
    const benchPlayer = player.parent.findOne(node => {
      return node.getType() === 'Group' && !node.attrs.firstDrag && node.name() === player.name();
    });

    const playerText = player.findOne(child => {
      return child.getClassName() === 'Text';
    }).text();

    benchPlayer.clearCache();
    const benchPlayerText = benchPlayer.findOne(child => {
      return child.getClassName() === 'Text';
    });

    if (benchPlayerText.text() > playerText) {
      benchPlayerText.text(playerText);
    }

    benchPlayer.cache({
      pixelRatio: 3,
      imageSmoothingEnabled: true,
    });
  }
}
