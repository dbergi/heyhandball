import { Injectable } from '@angular/core';
import {SocialSharing} from '@ionic-native/social-sharing/ngx';
import {FileDownloadService} from './file-download.service';
import {HelpersService} from './helpers.service';

@Injectable({
  providedIn: 'root'
})
export class SocialSharingService {

  constructor(
      private socialSharing: SocialSharing,
  ) { }

  public async share(dataUrl) {
    return await this.socialSharing.share(null, null, dataUrl);
  }
}
