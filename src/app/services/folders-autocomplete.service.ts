import { Injectable } from '@angular/core';
import { AutoCompleteService } from 'ionic4-auto-complete';

@Injectable({
  providedIn: 'root'
})
export class FoldersAutocompleteService implements AutoCompleteService {

  labelAttribute = null;

  private folders: any[] = [];

  constructor() { }

  setFolders(folders) {
    this.folders = folders;
  }

  getResults(keyword: string): Array<string> {
    keyword = keyword.toLowerCase();
    
    return this.folders.filter(folder => {
        const value = folder.toLowerCase();

        return value.includes(keyword);
      }
    );
  }
}
