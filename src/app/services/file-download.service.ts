import { Injectable } from '@angular/core';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { File } from '@ionic-native/file/ngx';
import {Platform} from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class FileDownloadService {

  constructor(private transfer: FileTransfer, private file: File, private platform: Platform) { }

  upload() {
    const fileTransfer: FileTransferObject = this.transfer.create();

    const options: FileUploadOptions = {
      fileKey: 'file',
      fileName: 'name.jpg',
      headers: {}
    }

    fileTransfer.upload('<file path>', '<api endpoint>', options)
        .then((data) => {
          // success
        }, (err) => {
          // error
        })
  }

  public async download(url, filename) {
    const fileTransfer: FileTransferObject = this.transfer.create();
    let directory = this.file.dataDirectory;

    if (this.platform.is('android')) {
      directory = this.file.externalDataDirectory;
    }

    return await fileTransfer.download(url, directory + filename + '.png').then((entry) => {
      console.log('download complete: ' + entry.toURL());
      return entry.toURL();
    });
  }
}
