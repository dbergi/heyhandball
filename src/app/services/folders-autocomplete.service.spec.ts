import { TestBed } from '@angular/core/testing';

import { FoldersAutocompleteService } from './folders-autocomplete.service';

describe('FoldersAutocompleteService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FoldersAutocompleteService = TestBed.get(FoldersAutocompleteService);
    expect(service).toBeTruthy();
  });
});
