import { Injectable, OnDestroy } from '@angular/core';
import {IAPProduct, InAppPurchase2} from '@ionic-native/in-app-purchase-2/ngx';
import {VersionService} from './version.service';
import {AdMob} from '@ionic-native/admob-plus/ngx';
import {Platform} from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class StoreService {

  private adTimeout;

  private subscriptions = [];

  constructor(
      private store: InAppPurchase2,
      private version: VersionService,
      private adMob: AdMob,
      private platform: Platform,
  ) {
    this.addAdMob();
  }

  public setStore() {
    this.store.verbosity = this.store.DEBUG;

    // Track all store errors
    this.store.error((err) => {
      console.error('Store Error ' + JSON.stringify(err));
    });

    this.store.validator =
        'https://validator.fovea.cc/v1/validate?appName=com.bergitech.heyhandball&apiKey=e5c9ebe9-f09c-45fd-a122-58861d999e89';
    this.store.register({
      id: 'billing.heyhandball',
      type: this.store.NON_CONSUMABLE
    });

    this.store.when('billing.heyhandball').owned((product: IAPProduct) => {
      this.unlockApp();
    });
    this.store.when('billing.heyhandball')
        .approved((p: IAPProduct) => p.verify())
        .verified((p: IAPProduct) => {
          console.log('product verified');
          p.finish();
        });

    this.store.refresh();
  }

  order(productId) {
    try {
      this.store.order(productId)
          .then(p => {
            console.log('Purchase succesful ' + JSON.stringify(p));
          })
          .catch(e => {
            console.log('Error ordering from store ' + e);
          });
    } catch (err) {
      console.log('Error ordering ' + JSON.stringify(err));
    }
  }

  private unlockApp() {
    console.log('unlocking app');
    clearTimeout(this.adTimeout);
    this.subscriptions.forEach(subscription => {
      subscription.unsubscribe();
    });

    this.version.fullVersion.next(true);
  }

  private addAdMob() {
    this.subscriptions.push(this.platform.pause.subscribe(() => {
      clearTimeout(this.adTimeout);
      console.log('background');
    }));

    this.subscriptions.push(this.platform.resume.subscribe(() => {
      this.showAd();
      console.log('foreground');
    }));

    window.addEventListener('beforeunload', () => {
      clearTimeout(this.adTimeout);
      console.log('unload app');
    });

    document.addEventListener('admob.interstitial.load_fail', (e) => {
      console.error(`Error showing ads...`, e);
      this.showAd();
    });

    this.adMob.setDevMode(false);
    this.showAd();
  }

  private showAd() {
    this.adTimeout = setTimeout(() => {
      this.adMob.interstitial.load({
        id: {
          // replace with your ad unit IDs
          android: 'ca-app-pub-1033412301725218/4390755396',
          ios: 'ca-app-pub-1033412301725218/4390755396',
        },
      }).then(() => this.adMob.interstitial.show());
    }, 150000);
  }
}
