import { Injectable } from '@angular/core';
import {BehaviorSubject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class VersionService {

  public fullVersion: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor() { }
}
