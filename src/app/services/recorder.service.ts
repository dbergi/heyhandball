import { Injectable } from '@angular/core';
import {File} from '@ionic-native/file/ngx';
import {TranslateService} from '@ngx-translate/core';
import {SocialSharingService} from './social-sharing.service';

@Injectable({
  providedIn: 'root'
})
export class RecorderService {

  private mediaRecorder = null;
  private blobs = [];

  constructor(
      private file: File,
      private translate: TranslateService,
      private socialShare: SocialSharingService,
  ) { }

  public async startRecording(layer): Promise<boolean> {
    this.blobs = [];

    return new Promise(resolve => {
      setTimeout(() => {

        const stream = layer.getStage().getCanvas()._canvas.captureStream(60);
        let options = {mimeType: 'video/webm'};
        try {
          this.mediaRecorder = new MediaRecorder(stream, options);
        } catch (e0) {
          console.log('Unable to create MediaRecorder with options Object: ', e0);
          try {
            options = {mimeType: 'video/webm,codecs=vp9'};
            this.mediaRecorder = new MediaRecorder(stream, options);
          } catch (e1) {
            console.log('Unable to create MediaRecorder with options Object: ', e1);
            try {
              options = {mimeType: 'video/vp8'};
              this.mediaRecorder = new MediaRecorder(stream, options);
            } catch (e2) {
              alert(this.translate.instant('tactical-board.error-recording'));
              console.error('Exception while creating MediaRecorder:', e2);
              resolve(false);
            }
          }
        }
        console.log('Created MediaRecorder', this.mediaRecorder, 'with options', options);

        this.mediaRecorder.ondataavailable = this.handleDataAvailable.bind(this);
        this.mediaRecorder.onstop = this.handleStop.bind(this);

        this.mediaRecorder.start();
        console.log('MediaRecorder started', this.mediaRecorder);

        resolve(true);
      }, 300);
    });
  }

  async stopRecording(): Promise<boolean> {
    this.mediaRecorder.stop();
    return new Promise(resolve => {
      setTimeout(() => {
        resolve(false);
      }, 300);
    });
  }

  private handleDataAvailable(event) {
    if (event.data && event.data.size > 0) {
      this.blobs.push(event.data);
    }
  }

  private async handleStop(event) {
    console.log('Recorder stopped: ', event);
    const superBuffer = new Blob(this.blobs, {type: 'video/mp4'});
    const filename = 'heyhandball-animation-tmp-' + (new Date()).getTime();

    this.file.writeFile(this.file.cacheDirectory, filename + '.mp4', superBuffer)
        .then(file => {
          console.log('write successful', file);
          this.socialShare.share(file.nativeURL);
        })
        .catch(err => console.log(err + ' write failed'));
  }
}
