import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HelpersService {

  constructor() { }

  public getDate() {
    const date = new Date();

    const day = date.getDate();
    const month = date.getMonth() + 1;
    const year = date.getFullYear();
    const hour = date.getHours();
    const minute = date.getMinutes();
    const seconds = date.getSeconds();

    if (month < 10) {
      return day + '0' + month + '' + year + '' + hour + '' + minute + '' + seconds;
    } else {
      return day + '' + month + '' + year  + '' + hour + '' + minute + '' + seconds;
    }
  }
}
