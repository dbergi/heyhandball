import { Component, OnInit } from '@angular/core';
import {ModalController, Platform} from '@ionic/angular';
import {StoreService} from "../../services/store.service";

@Component({
  selector: 'app-order',
  templateUrl: './order.page.html',
  styleUrls: ['./order.page.scss'],
})
export class OrderPage implements OnInit {

  constructor(
    private store: StoreService,
    private modal: ModalController,
  ) { }

  ngOnInit() {
  }

  order() {
    this.store.order('billing.heyhandball');
    this.modal.dismiss();
  }
}
