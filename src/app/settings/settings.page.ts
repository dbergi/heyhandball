import { Component, OnInit } from '@angular/core';
import {ModalController} from '@ionic/angular';
import {OrderPage} from './order/order.page';
import {VersionService} from '../services/version.service';
import {InAppPurchase2} from '@ionic-native/in-app-purchase-2/ngx';
import {SettingsService} from '../services/settings.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage {

  public toggleColorPickerA = false;
  public toggleColorPickerB = false;
  private subscription;
  public settings;

  constructor(
    private modal: ModalController,
    public version: VersionService,
    private store: InAppPurchase2,
    public settingsService: SettingsService,
  ) {
    this.subscription = this.settingsService.watch().subscribe(settings => {
      this.settings = settings;
    });
  }

  public async order() {
    const modal = await this.modal.create({
      component: OrderPage,
      cssClass: 'orderModal'
    });

    await modal.present();
    await modal.onDidDismiss();
  }

  public async reloadPurchase() {
      this.store.refresh();
  }
}
